import React, { useState } from "react";
import { Hello } from "./Hello";

const App = () => {
  const [count, setCount] = useState(0);

  return (
    <div>
      <Hello increment={() => setCount(count + 1)} />
      <div>count: {count}</div>
    </div>
  );
};

export default App;

//import { useEffect, useLayoutEffect, useRef, useState } from "react";
//import "./App.css";
//import { useForm } from "./useForm";
//import { useFetch } from "./useFetch";
//import Hello from "./Hello";

// function App() {
//   const [values, handleChange] = useForm({ email: "", password: "" });

//   const [count, setCount] = useState(() =>
//     JSON.parse(localStorage.getItem("count"))
//   );
//   const { data, loading } = useFetch(`http://www.numberapi.com/${count}`);
//   const inputRef = useRef();

//   const [showHello, setShowHello] = useState(true);

//   useEffect(() => {
//     localStorage.setItem("count", JSON.stringify(count));
//   }, [count]);

//   useLayoutEffect(() => {
//     console.log(inputRef.current.getBoundingClientRect());
//   }, []);

// useEffect(() => {
//   const onMouseMove = e => {
//     console.log(e);
//   };
//   window.addEventListener("mousemove", onMouseMove);

//   return () => {
//     window.removeEventListener("mosemove", onMouseMove);
//   };
// }, []);

// useEffect(() => {
//   console.log("mount1");
// }, []);

// useEffect(() => {
//   console.log("mount2");
// }, []);

//   return (
//     <div className="App">
//       <div>{loading ? "loading..." : data}</div>
//       <div>count: {count}</div>
//       <button onClick={() => setCount(c => c + 1)}>increment</button>
//       <hr></hr>
//       <button onClick={() => setShowHello(!showHello)}>toggle</button>
//       {showHello && <Hello />}
//       <div>
//         <input
//           ref={inputRef}
//           name="email"
//           value={values.email}
//           onChange={handleChange}
//         />
//         <input
//           name="firstName"
//           placeholder="first name"
//           value={values.firstName}
//           onChange={handleChange}
//         />
//         <button
//           onClick={() => {
//             inputRef.current.focus();
//           }}
//         >
//           focus
//         </button>
//       </div>
//     </div>
//   );
// }

// export default App;
