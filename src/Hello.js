import React from "react";

export const Hello = ({ increment }) => {
  return <button onClick={increment}>Hello</button>;
};

// import React, { useRef } from "react";

// const Hello = () => {
//   const renders = useRef(0);

//   console.log("hello renders:", renders.current++);

//   return <div>Hello</div>;
// };

// export default Hello;
